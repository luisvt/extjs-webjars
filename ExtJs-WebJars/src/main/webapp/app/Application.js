Ext.define('myapp.Application', {
    name: 'myapp',

    extend: 'Ext.app.Application',

    views: [
        'customer.List',
        'customer.Edit'
    ],

    controllers: [
        'Customers'
    ],

    stores: [
        'Customers'
    ]
});
