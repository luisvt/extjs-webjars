Ext.define('myapp.store.Customers', {
    extend: 'Ext.data.Store',
    model: 'myapp.model.Customer',
    autoLoad: true,
    
    proxy: {
        type: 'ajax',
        url: 'json/customers.json'
    }
});