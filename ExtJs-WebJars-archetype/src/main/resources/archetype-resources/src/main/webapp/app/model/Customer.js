Ext.define('myapp.model.Customer', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'active', type: 'boolean' },
        { name: 'createDate', type: 'int' },
        { name: 'customerId', type: 'int' },
        { name: 'email', type: 'string' },
        { name: 'firstName', type: 'string' },
        { name: 'lastName', type: 'string' },
        { name: 'lastUpdate', type: 'int' }
    ]
});
