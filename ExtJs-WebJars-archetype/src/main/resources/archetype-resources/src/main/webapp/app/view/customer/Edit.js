Ext.define("myapp.view.customer.Edit", {
    extend: 'Ext.window.Window',
    xtype: 'customerEdit',
    title: 'Edit Customer',
    layout: 'fit',
    autoShow: true,
    
    items: [
        {
            xtype: 'form',
            items: [
                {xtype: 'textfield', name: 'firstName', fieldLabel: 'First Name'},
                {xtype: 'textfield', name: 'email', fieldLabel: 'Email'}
            ],
            buttons: [
                {
                    text: 'Save',
                    action: 'save'
                }
            ]
        }
    ]
});