Ext.define("myapp.view.customer.List", {
    extend: 'Ext.grid.Panel',
    xtype: 'customerList',
    title: 'Customers',
    store: 'Customers',
    columns: [
        {header: 'Name', dataIndex: 'firstName', flex: 1},
        {header: 'Name', dataIndex: 'email', flex: 2}
    ]
});