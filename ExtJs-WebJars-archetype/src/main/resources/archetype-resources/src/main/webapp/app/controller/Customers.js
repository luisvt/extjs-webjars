Ext.define('myapp.controller.Customers', {
    extend: 'Ext.app.Controller',
    stores: [
        'Customers'
    ],
    init: function() {
        var me = this;
        me.control({
            'customerList': {
                itemdblclick: me.editCustomer
            },
            
            'customerEdit button[action=save]': {
                click: me.updateCustomer
            },
            
            '#syncCustomer': {
                click: me.syncCustomer
            }
        });
    },
    
    editCustomer: function(grid, record) {
        var view = Ext.widget('customerEdit');
        
        view.down('form').loadRecord(record);
    },
        
    updateCustomer: function(button) {
        var win = button.up('window'),
            form = win.down('form'),
            record = form.getRecord(),
            values = form.getValues();
        
        record.set(values);
        win.close();
    },
        
    syncCustomer: function() {
        this.getCustomersStore().sync();
    }
});
