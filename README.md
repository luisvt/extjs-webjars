# ExtJs-WebJars

This is a sample application created to demonstrate how to use the framework Extjs and Maven Webjars.

As you can see in the maven POM file to use Extjs you only need to add ExtJs-webjar dependency:

```xml
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>extjs</artifactId>
    <version>4.2.1.883</version>
</dependency>
```

And finally to use this webjar into an Html website you only need to add the script tag and the import the respective stylesheet into the index.html as fallow:

```html
<link rel="stylesheet" href="webjars/extjs/4.2.1.883/packages/ext-theme-classic/build/resources/ext-theme-classic-all.css">
<script src="webjars/extjs/4.2.1.883/ext-dev.js"></script>
```